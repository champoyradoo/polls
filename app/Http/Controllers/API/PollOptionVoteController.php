<?php

namespace App\Http\Controllers\API;

use App\PollOptionVote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PollOptionVoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'poll_option_id' => 'required|numeric'
        ]);

        $vote = PollOptionVote::create([
            'poll_option_id' => $data['poll_option_id']
        ]);

        return response()->json($vote);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PollOptionVote  $pollOptionVote
     * @return \Illuminate\Http\Response
     */
    public function show(PollOptionVote $pollOptionVote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PollOptionVote  $pollOptionVote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PollOptionVote $pollOptionVote)
    {
        $data = $request->validate(['poll_option_id' => 'required|numeric']);

        $pollOptionVote->poll_option_id = $data['poll_option_id'];
        $pollOptionVote->save();

        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PollOptionVote  $pollOptionVote
     * @return \Illuminate\Http\Response
     */
    public function destroy(PollOptionVote $pollOptionVote)
    {
        //
    }
}
