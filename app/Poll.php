<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Model\HasCreatedBy;
use App\PollOption;

class Poll extends Model
{
	use HasCreatedBy;

	protected $fillable = ['title'];

	public function options()
	{
		return $this->hasMany(PollOption::class);
	}

	public function votedOption()
	{
		
	}
}
