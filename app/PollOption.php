<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Model\HasCreatedBy;
use App\Poll;

class PollOption extends Model
{
	use HasCreatedBy;

	protected $fillable = ['name'];

	public function poll()
	{
		return $this->belongsTo(Poll::class);
	}

	public function votes()
	{
		return $this->hasMany(\App\PollOptionVote::class);
	}
}
