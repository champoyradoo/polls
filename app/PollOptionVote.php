<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Model\HasCreatedBy;

class PollOptionVote extends Model
{
	use HasCreatedBy;

	protected $fillable = ['poll_option_id'];

	public function option()
	{
		return $this->belongsTo(\App\PollOption::class);
	}
}
