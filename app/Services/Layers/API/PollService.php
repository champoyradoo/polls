<?php

namespace App\Services\Layers\API;


class PollService
{
    public function getUniqueOptions(array $options)
    {
        $uniqueNames = [];

        return array_filter($options, function($data) use (&$uniqueNames) {
            $name = strtolower($data['name']);

            if (in_array($name, $uniqueNames)) {
                return false;
            }

            $uniqueNames[] = $name;

            return true;
        });
    }
}