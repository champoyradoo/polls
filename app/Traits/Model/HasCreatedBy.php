<?php

namespace App\Traits\Model;

trait HasCreatedBy
{
	public static function boot()
	{
		parent::boot();

		static::creating(function($model) {
			$model->created_by = ($model->created_by) ?? \Auth::user()->id;
		});
	}
}