<?php

use Faker\Generator as Faker;

$factory->define(App\Poll::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'created_by' => function() {
            return factory(App\User::class)->create()->id;
        }
    ];
});
