<?php

use Faker\Generator as Faker;

$factory->define(App\PollOption::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'poll_id' => function() {
            return factory(\App\Poll::class)->create()->id;
        },
        'created_by' => function() {
            return factory(\App\User::class)->create()->id;
        }
    ];
});
