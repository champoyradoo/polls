<?php

use Faker\Generator as Faker;

$factory->define(App\PollOptionVote::class, function (Faker $faker) {
    return [
        'poll_option_id' => function() {
            return factory(\App\PollOption::class)->create()->id;
        },
        'created_by' => function() {
            return factory(\App\User::class)->create()->id;
        }
    ];
});
