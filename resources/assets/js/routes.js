import store from './store/store';

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/polls')
}

const ifAuthenticated = (to, from, next) => {
	if (! store.getters.isAuthenticated) {
		next('/login');
		return;
	}
	
	if (!! store.getters.user.id) {
		next();
		return;
	}

	// wait for the user data to be set
	let unwatch = store.watch(
      (store, getters) => getters.user,
      value => {
      	if (!! value.id) {
      		unwatch();
      		next();
      	}
      }
    );
}


export default [
	{
		path: '/', 
		component: require('./components/home/Home.vue')
	},
	{
		path: '/register',
		component: require('./components/auth/Register.vue'),
		beforeEnter: ifNotAuthenticated
	},
	{
		path: '/login',
		component: require('./components/auth/Login.vue'),
		beforeEnter: ifNotAuthenticated
	},
	{
		path: '/polls',
		component: require('./components/polls/Polls.vue'),
		beforeEnter: ifAuthenticated
	},
	{
		path: '/polls/:id',
		component: require('./components/polls/SinglePoll.vue'),
		beforeEnter: ifAuthenticated
	},
	{
		path: '/add-poll',
		component: require('./components/polls/AddPoll.vue'),
		beforeEnter: ifAuthenticated
	}
];