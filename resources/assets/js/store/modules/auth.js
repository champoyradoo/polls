import client from '../../data/client.js';

const state = {
	token: '',
	status: '',
	user: {}
};

const getters = {
	isAuthenticated: state => !!state.token,
	authStatus: state => state.status,
	user: state => state.user
};

const mutations = {
	'AUTH_REQUEST': (state) => {
		state.status = 'loading'
	},
	'AUTH_SUCCESS': (state, token) => {
		state.status = 'success';
		state.token = token;
	},
	'AUTH_ERROR': (state) => {
		state.status = 'error'
	},
	'AUTH_LOGOUT': (state) => {
		state.token = '';
	},
	'AUTH_USER': (state, user) => {
		state.user = user;
	}
};

const actions = {
	'AUTH_REQUEST': ({commit, dispatch}, user) => {
		return new Promise((resolve, reject) => { // The Promise used for router redirect in login
	      commit('AUTH_REQUEST')

	      let data = {
	      	grant_type: 'password',
	      	client_id: client.id,
	      	client_secret: client.secret,
	      	username: user.email,
	      	password: user.password
	      };

	      axios({url: '/oauth/token', data, method: 'POST'})
	        .then(resp => {
	        	const token = resp.data.access_token;
	        	dispatch('AUTH_LOGIN', token);
	          	resolve(resp);
	        })
	      	.catch(err => {
		        commit('AUTH_ERROR', err);
		        localStorage.removeItem('user-token');
		        reject(err);
	      	})
	    });
	},
	'AUTH_LOGIN': ({commit, dispatch, state}, token) => {
		localStorage.setItem('user-token', token); // store the token in localstorage
      	axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
      	commit('AUTH_SUCCESS', token);
      	axios.get('/api/user')
      		.then(resp => {
      			commit('AUTH_USER', resp.data);
      		}).catch(err => {
      			commit('AUTH_LOGOUT');
      		});
	},
	'AUTH_LOGOUT': ({commit, dispatch, state}) => {
		return new Promise((resolve, reject) => {
			axios.post('/api/logout')
				.then(resp => {
					commit('AUTH_LOGOUT');
					localStorage.removeItem('user-token');
					delete axios.defaults.headers.common['Authorization'];
					resolve(resp);
				})
				.catch(err => {
					reject(err);
				});
		});
	}
};

export default {
	state,
	getters,
	mutations,
	actions
};
