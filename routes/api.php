<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'Auth\RegisterController@register');
Route::post('/logout', 'Auth\LoginController@apiLogout');

Route::get('/polls', [\App\Http\Controllers\API\PollController::class, 'index']);

Route::middleware('auth:api')->group(function() {
	Route::get('/user', function (Request $request) {
    	return $request->user();
	});

    Route::prefix('polls')->group(function() {
        Route::post('/', [\App\Http\Controllers\API\PollController::class, 'store']);
        Route::get('/{poll}', [\App\Http\Controllers\API\PollController::class, 'show']);
    });

	Route::resource('poll_option_votes', 'API\PollOptionVoteController');
});

