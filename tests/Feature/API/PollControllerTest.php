<?php

namespace Tests\Feature\API;

use App\Poll;
use App\PollOption;
use App\PollOptionVote;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PollControllerTest extends TestCase
{
    const STORE_URI = '/api/polls';

    use RefreshDatabase;

    /**
     *
     */
    public function testStoreAddsPollAndOptions()
    {
        // arrange
        $user = factory(User::class)->create();
        $data = [
            'title' => 'Some Title',
            'options' => [
                ['name' => 'Option 1'],
                ['name' => 'Option 2']
            ]
        ];

        // act
        $response = $this->actingAs($user, 'api')->post(self::STORE_URI, $data);

        // assert
        $response->assertStatus(200);
        $this->assertDatabaseHas('polls', [
            'title' => $data['title'],
            'created_by' => $user->id
        ]);
        // check that all the options has been stored
        $poll = Poll::where('title', $data['title'])->first();

        foreach ($data['options'] as $i => $option) {
            $this->assertDatabaseHas('poll_options', [
                'name' => $data['options'][$i]['name'],
                'poll_id' => $poll->id,
                'created_by' => $user->id
            ]);
        }
    }

    public function testStoreAddOnlyUniqueOptions()
    {
        // arrange
        $user = factory(User::class)->create();
        $data = [
            'title' => 'Some Title',
            'options' => [
                ['name' => 'Option 1'],
                ['name' => 'Option 2'],
                ['name' => 'Option 1'],
                ['name' => 'option 2']
            ]
        ];

        $response = $this->actingAs($user, 'api')->post(self::STORE_URI, $data);

        // assert
        $response->assertStatus(200);
        // check that there are only 2 polls
        $poll = Poll::where('title', $data['title'])->first();

        $this->assertCount(2, $poll->options);
    }

    public function testShowPoll()
    {
        $user = factory(User::class)->create();
        $poll = factory(Poll::class)->create();

        // save first option
        $firstPollOption = factory(PollOption::class)->make(['poll_id' => null]);
        $poll->options()->save($firstPollOption);

        // add vote to first option
        $firstPollOptionVote = factory(PollOptionVote::class)->make(['poll_option_id' => null]);
        $firstPollOption->votes()->save($firstPollOptionVote);

        // add second option
        $poll->options()->save(factory(PollOption::class)->make(['poll_id' => null]));

        $response = $this->actingAs($user, 'api')->json('GET', "/api/polls/{$poll->id}");

        $response->assertStatus(200)
            ->assertExactJson($poll->load('options.votes')->toArray());
        // assert that the first option has a vote
        $responseData = $response->decodeResponseJson();
        $this->assertCount(1, $responseData['options'][0]['votes']);
    }

    /** @test */
    public function get_options_allows_guest()
    {
        $polls = factory(Poll::class, 2)->create();

        $response = $this->getJson('/api/polls')
            ->assertSuccessful();

        $this->assertCount(2, $response->json('data'));
    }
}
