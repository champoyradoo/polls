<?php

namespace Tests\Unit\Services\Layers\API;

use App\Services\Layers\API\PollService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PollServiceTest extends TestCase
{
    protected $service;

    public function setUp()
    {
        parent::setUp();

        $this->service = new PollService();
    }

    public function testGetUniqueOptions()
    {
        $data = [
            ['name' => 'Option 1'],
            ['name' => 'Option 2'],
            ['name' => 'Option 1'],
            ['name' => 'option 2']
        ];

        $result = $this->service->getUniqueOptions($data);

        $this->assertEquals(
            [
                ['name' => 'Option 1'],
                ['name' => 'Option 2']
            ],
            $result
        );
    }
}
